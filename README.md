# README #

### Summary ###

This was a take home assignment for an interview. The requirements for the assignment were:

-	Provide a UI for the user to enter numbers one at a time, and show the sequence of numbers in a text field. 
-	The UI must have a button that, when pressed, sorts the sequence of numbers and updates the text field accordingly. 
-	The UI must have an input field, where the user must enter one number from the provided sequence, and a button that, when pressed, finds and displays the index of the provided number in the sorted list. 
-	The application must adapt to both portrait and landscape orientations. 
-	The application must be easy and intuitive to use, and prevent the user from doing unexpected tasks. 
-	The application must follow UI guidelines and common conventions of the chosen platform. - 	The application should ideally handle edge cases 

### Version ###

This is version 1. I will not be making any additional changes to this application.

### How do I get set up? ###

Just pull and run. The project has no dependencies.