//
//  ViewController.swift
//  NumberInput
//
//  Created by Sumeet Gill on 2016-07-04.
//  Copyright © 2016 Sumeet Gill. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var txtNumbers: UITextField!
    @IBOutlet var btnFind: UIButton!
    
    // MARK: Class Variables
    var isFind: Bool = false;
    var numberArray:Array = [Int]();
    
    // MARK: System functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Data Handling
    
    /// Handles the input from the user
    @IBAction func inputNumber(sender: AnyObject){
            
        if isFind {
            findNumber(sender.tag)
        } else {
            numberArray.append(sender.tag)
            updateTextBoxWithArray()
        }
    }
    
    
    /// Returns the index of the searched number, if it exists
    func findNumber(numberToFind:Int) {
        
        updateFindState()
        
        guard let indexOfNumber:Int = numberArray.indexOf(numberToFind) else {
            showAlert("Index not found for number")
            return
        }
    
        showAlert("Index of number: \(indexOfNumber)")
    }

    /// Sorts the array in descending order
    @IBAction func sortNumbers(sender: AnyObject) {
        
        numberArray.sortInPlace()
        
        updateTextBoxWithArray()
    }
    
    /// Returns the app to default state
    @IBAction func clearData(sender: AnyObject) {
    
        numberArray.removeAll()
        txtNumbers.text = ""
        isFind = false
        
        showAlert("Data Cleared")
    }
    
    // MARK: UI Functions
    
    /// Changes isFind to opposite state and updates the button colour to reflect
    @IBAction func updateFindState() {
        
        isFind = !isFind
        
        if isFind {
            btnFind.tintColor = UIColor.redColor()
        } else {
            btnFind.tintColor = self.view.tintColor // easiest way to return to default colour
        }
    }
    
    func updateTextBoxWithArray() {
        
        var numberString:String = ""
        
        for index in numberArray {
            numberString += String(index)
        }
        
        txtNumbers.text = numberString
    }
    
    /// Displays an alertview with "OK" option
    func showAlert(text: String) {
        
        let alert = UIAlertController(title: "Alert", message: text, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
        self.presentViewController(alert, animated: true, completion: nil)
    }

}

